select /*+  NO_MERGE  */ /* 2a.103 */
       owner,
       SUM(CASE WHEN TRIM(degree) = 'DEFAULT' THEN 1 ELSE 0 END) "DEFAULT",
       SUM(CASE WHEN TRIM(degree) = '0' THEN 1 ELSE 0 END) "0",
       SUM(CASE WHEN TRIM(degree) = '1' THEN 1 ELSE 0 END) "1",
       SUM(CASE WHEN TRIM(degree) = '2' THEN 1 ELSE 0 END) "2",
       SUM(CASE WHEN TRIM(degree) IN ('3', '4') THEN 1 ELSE 0 END) "3-4",
       SUM(CASE WHEN TRIM(degree) IN ('5', '6', '7', '8') THEN 1 ELSE 0 END) "5-8",
       SUM(CASE WHEN TRIM(degree) IN ('9', '10', '11', '12', '13', '14', '15', '16') THEN 1 ELSE 0 END) "9-16",
       SUM(CASE WHEN LENGTH(TRIM(degree)) = 2 AND TRIM(degree) BETWEEN '17' AND '32' THEN 1 ELSE 0 END) "17-32",
       SUM(CASE WHEN LENGTH(TRIM(degree)) = 2 AND TRIM(degree) BETWEEN '33' AND '64' THEN 1 ELSE 0 END) "33-64",
       SUM(CASE WHEN (LENGTH(TRIM(degree)) = 2 AND TRIM(degree) BETWEEN '65' AND '99') OR
                     (LENGTH(TRIM(degree)) = 3 AND TRIM(degree) BETWEEN '100' AND '128') THEN 1 ELSE 0 END) "65-128",
       SUM(CASE WHEN LENGTH(TRIM(degree)) = 3 AND TRIM(degree) BETWEEN '129' AND '256' THEN 1 ELSE 0 END) "129-256",
       SUM(CASE WHEN LENGTH(TRIM(degree)) = 3 AND TRIM(degree) BETWEEN '257' AND '512' THEN 1 ELSE 0 END) "257-512",
       SUM(CASE WHEN (LENGTH(TRIM(degree)) = 3 AND TRIM(degree) > '512') OR
                     (LENGTH(TRIM(degree)) > 3 AND TRIM(degree) != 'DEFAULT') THEN 1 ELSE 0 END) "HIGHER"
  FROM dba_tables
 WHERE owner IN ('<SCHEMA_VAL>')
HAVING COUNT(*) > SUM(CASE WHEN TRIM(degree) IN ('0', '1') THEN 1 ELSE 0 END)
 GROUP BY
       owner
 ORDER BY
       owner
