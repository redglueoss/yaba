SELECT OWNER
	,TABLE_NAME AS TABLE_NAME
	,COLUMN_NAME AS COLUMN_NAME
	,data_type
	,data_length
	,data_precision
	,data_scale
	,CASE 
		WHEN DATA_TYPE = 'VARCHAR2'
			THEN ('STRING')
		WHEN DATA_TYPE = 'DATE'
			THEN ('TIMESTAMP')
		WHEN DATA_TYPE = 'NUMBER'
			AND DATA_PRECISION IS NULL
			AND DATA_LENGTH >= 19
			THEN ('DECIMAL(' || data_length || ',0)')
		WHEN DATA_TYPE = 'NUMBER'
			AND DATA_SCALE = 0
			AND DATA_PRECISION < 10
			THEN ('INT')
		WHEN DATA_TYPE = 'NUMBER'
			AND DATA_SCALE = 0
			AND DATA_PRECISION < 19
			AND DATA_PRECISION >= 10
			THEN ('BIGINT')
		WHEN DATA_TYPE = 'NUMBER'
			AND (
				DATA_SCALE > 0
				OR DATA_PRECISION >= 19
				)
			THEN ('DECIMAL(' || data_precision || ',' || data_scale || ')')
		WHEN DATA_TYPE = 'BINARY_DOUBLE'
			THEN ('DOUBLE')
		WHEN DATA_TYPE = 'BINARY_FLOAT'
			THEN ('FLOAT')
		WHEN DATA_TYPE = 'RAW'
			THEN ('BINARY')
		ELSE 'UNDEFINED'
		END DELTA_DATA_TYPE
FROM (
	SELECT dba_tab_cols.OWNER
		,dba_tab_cols.table_name
		,dba_tab_cols.column_name
		,dba_tab_cols.data_type
		,dba_tab_cols.data_length
		,dba_tab_cols.data_precision
		,dba_tab_cols.data_scale
	FROM dba_tab_cols
	WHERE dba_tab_cols.OWNER IN (
			'<SCHEMA_VAL>'
			) and DATA_TYPE = 'NUMBER'
	) 