SELECT /*+  NO_MERGE  */ /* 1a.18 */
       *
  FROM dba_feature_usage_statistics
 ORDER BY
       name,
       version
