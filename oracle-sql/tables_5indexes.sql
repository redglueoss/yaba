SELECT /*+  NO_MERGE  */ /* 2a.90 */
       COUNT(*) indexes,
       table_owner,
       table_name,
       SUM(CASE WHEN index_type LIKE 'NORMAL%' THEN 1 ELSE 0 END) type_normal,
       SUM(CASE WHEN index_type LIKE 'BITMAP%' THEN 1 ELSE 0 END) type_bitmap,
       SUM(CASE WHEN index_type LIKE 'FUNCTION-BASED%' THEN 1 ELSE 0 END) type_fbi,
       SUM(CASE WHEN index_type LIKE 'CLUSTER%' THEN 1 ELSE 0 END) type_cluster,
       SUM(CASE WHEN index_type LIKE 'IOT%' THEN 1 ELSE 0 END) type_iot,
       SUM(CASE WHEN index_type LIKE 'DOMAIN%' THEN 1 ELSE 0 END) type_domain,
       SUM(CASE WHEN index_type LIKE 'LOB%' THEN 1 ELSE 0 END) type_lob,
       SUM(CASE WHEN partitioned LIKE 'YES%' THEN 1 ELSE 0 END) partitioned,
       SUM(CASE WHEN temporary LIKE 'Y%' THEN 1 ELSE 0 END) temporary,
       SUM(CASE WHEN uniqueness LIKE 'UNIQUE%' THEN 1 ELSE 0 END) is_unique,
       SUM(CASE WHEN uniqueness LIKE 'NONUNIQUE%' THEN 1 ELSE 0 END) non_unique,
       SUM(CASE WHEN status LIKE 'VALID%' THEN 1 ELSE 0 END) valid,
       SUM(CASE WHEN status LIKE 'N/A%' THEN 1 ELSE 0 END) status_na,
       SUM(CASE WHEN visibility LIKE 'VISIBLE%' THEN 1 ELSE 0 END) visible,
       SUM(CASE WHEN visibility LIKE 'INVISIBLE%' THEN 1 ELSE 0 END) invisible,
       SUM(CASE WHEN status LIKE 'UNUSABLE%' THEN 1 ELSE 0 END) unusable
  FROM dba_indexes
 WHERE table_owner NOT IN ('ANONYMOUS','APEX_030200','APEX_040000','APEX_040200','APEX_SSO','APPQOSSYS','CTXSYS','DBSNMP','DIP','EXFSYS','FLOWS_FILES','MDSYS','OLAPSYS','ORACLE_OCM','ORDDATA','ORDPLUGINS','ORDSYS','OUTLN','OWBSYS')
   AND table_owner NOT IN ('SI_INFORMTN_SCHEMA','SQLTXADMIN','SQLTXPLAIN','SYS','SYSMAN','SYSTEM','TRCANLZR','WMSYS','XDB','XS$NULL','PERFSTAT','STDBYPERF','MGDSYS','OJVMSYS')
 GROUP BY
       table_owner,
       table_name
HAVING COUNT(*) > 5
 ORDER BY
       1 DESC