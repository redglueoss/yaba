WITH pc AS (
SELECT owner, name table_name
,      LISTAGG(column_name,', ') WITHIN GROUP (ORDER BY column_position) part_column_list
FROM   dba_part_key_columns
WHERE  object_type = 'TABLE'
GROUP BY owner, name
), sc as (
SELECT owner, name table_name
,      LISTAGG(column_name,', ') WITHIN GROUP (ORDER BY column_position) subp_column_list
FROM   dba_subpart_key_columns
WHERE  object_type = 'TABLE'
GROUP BY owner, name
), tp as (
SELECT table_owner owner, table_name
,      COUNT(*) part_count
,      COUNT(distinct tablespace_name) part_tablespaces
,      SUM(DECODE(compression,'ENABLE',1)) part_compression_enabled
,      SUM(CASE WHEN compress_for like 'BASIC'         THEN 1 END) part_compressfor_basic
,      SUM(CASE WHEN compress_for like 'QUERY HIGH%'   THEN 1 END) part_compressfor_queryhigh
,      SUM(CASE WHEN compress_for like 'QUERY LOW%'    THEN 1 END) part_compressfor_querylow
,      SUM(CASE WHEN compress_for like 'ARCHIVE HIGH%' THEN 1 END) part_compressfor_archivehigh
,      SUM(CASE WHEN compress_for like 'ARCHIVE LOW%'  THEN 1 END) part_compressfor_archivelow
,      SUM(num_rows) part_num_rows
,      SUM(blocks) part_blocks
,      SUM(DECODE(interval,'YES',1)) part_intervals
,      SUM(DECODE(segment_created,'YES',1)) part_segments_created
FROM   dba_tab_partitions
GROUP BY table_owner, table_name
), sp as (
SELECT table_owner owner, table_name
,      COUNT(*) subp_count
,      COUNT(distinct tablespace_name) subp_tablespaces
,      SUM(DECODE(compression,'ENABLE',1)) subp_compression_enabled
,      SUM(CASE WHEN compress_for like 'BASIC'         THEN 1 END) subp_compressfor_basic
,      SUM(CASE WHEN compress_for like 'QUERY HIGH%'   THEN 1 END) subp_compressfor_queryhigh
,      SUM(CASE WHEN compress_for like 'QUERY LOW%'    THEN 1 END) subp_compressfor_querylow
,      SUM(CASE WHEN compress_for like 'ARCHIVE HIGH%' THEN 1 END) subp_compressfor_archivehigh
,      SUM(CASE WHEN compress_for like 'ARCHIVE LOW%'  THEN 1 END) subp_compressfor_archivelow
,      SUM(num_rows) subp_num_rows
,      SUM(blocks) subp_blocks
,      SUM(DECODE(interval,'YES',1)) subp_intervals
,      SUM(DECODE(segment_created,'YES',1)) subp_segments_created
FROM   dba_tab_subpartitions
GROUP BY table_owner, table_name
)
SELECT /*+  NO_MERGE  */ /* 3c.268 */
  p.owner, p.table_name
, p.partitioning_type
, pc.part_column_list,tp.part_count, tp.part_intervals
,tp.part_num_rows, tp.part_blocks, tp.part_segments_created
,tp.part_tablespaces
,tp.part_compression_enabled
, NULLIF(p.subpartitioning_type,'NONE') subpartitioning_type
--,DEF_SUBPARTITION_COUNT
, sc.subp_column_list,sp.subp_count, sp.subp_intervals
,sp.subp_num_rows, sp.subp_blocks, sp.subp_segments_created
,sp.subp_tablespaces
,sp.subp_compression_enabled
--,STATUS
--,DEF_COMPRESSION
--,DEF_COMPRESS_FOR
--,IS_NESTED
  FROM dba_part_Tables p
       LEFT OUTER JOIN pc ON pc.owner = p.owner AND pc.table_name = p.table_name
       LEFT OUTER JOIN sc ON sc.owner = p.owner AND sc.table_name = p.table_name
       LEFT OUTER JOIN tp ON tp.owner = p.owner AND tp.table_name = p.table_name
       LEFT OUTER JOIN sp ON sp.owner = p.owner AND sp.table_name = p.table_name
 WHERE p.owner IN ('<SCHEMA_VAL>')
   AND p.owner IN ('<SCHEMA_VAL>')
   AND p.table_name NOT LIKE 'BIN%'
 ORDER BY
       p.owner, p.table_name