SELECT /*+  NO_MERGE  */ /* 2a.135 */
       COUNT(*) columns,
       c.owner,
       c.table_name,
       t.avg_row_len
  FROM dba_tab_columns c,
       dba_tables t
 WHERE c.owner NOT IN ('ANONYMOUS','APEX_030200','APEX_040000','APEX_040200','APEX_SSO','APPQOSSYS','CTXSYS','DBSNMP','DIP','EXFSYS','FLOWS_FILES','MDSYS','OLAPSYS','ORACLE_OCM','ORDDATA','ORDPLUGINS','ORDSYS','OUTLN','OWBSYS')
   AND c.owner NOT IN ('SI_INFORMTN_SCHEMA','SQLTXADMIN','SQLTXPLAIN','SYS','SYSMAN','SYSTEM','TRCANLZR','WMSYS','XDB','XS$NULL','PERFSTAT','STDBYPERF','MGDSYS','OJVMSYS')
   AND c.table_name NOT LIKE 'BIN%'
   AND t.owner = c.owner
   AND t.table_name = c.table_name
   AND NOT EXISTS
       (SELECT NULL FROM dba_views v WHERE v.owner = c.owner AND v.view_name = c.table_name)
 GROUP BY
       c.owner, c.table_name, t.avg_row_len
HAVING COUNT(*) > 255
 ORDER BY
       1 DESC,
       c.owner,
       c.table_name
