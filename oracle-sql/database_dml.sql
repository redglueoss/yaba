select table_owner,table_name,count(INSERTS) as number_of_inserts,count(UPDATES) as number_of_updates,count(DELETES) as number_of_deletes, count(UPDATES)+count(DELETES) as total_update_delete 
FROM dba_tab_modifications
where table_owner in ('<SCHEMA_VAL>')
group by table_owner,table_name 
order by total_update_delete desc