SELECT
    round((RATIO_TO_REPORT(io_read_ops) OVER()) * 100, 2) contribution_to_io_perc,
    round((RATIO_TO_REPORT(cpu_time) OVER()) * 100, 2) contribution_to_cpu_perc,
    round(elapsed_time / executions / 1000000) avg_exec_time_sec,
    a.*,
    (
        SELECT
            sql_text
        FROM
            dba_hist_sqltext
        WHERE
            sql_id = a.sql_id
            AND ROWNUM < 2
    ) sql_text
FROM
    (
        SELECT
            sql_id,
            plan_hash_value,
                        parsing_schema_name,
            module,
            SUM(executions_delta) executions,
            SUM(buffer_gets_delta) buffer_gets,
            SUM(elapsed_time_delta) elapsed_time,
            SUM(cpu_time_delta) cpu_time,
            SUM(disk_reads_delta) io_read_ops

        FROM
            dba_hist_sqlstat a
        WHERE
            snap_id IN (
                SELECT
                    snap_id
                FROM
                    dba_hist_snapshot
                WHERE
                    begin_interval_time >= trunc(SYSDATE) - 30
            ) 
--                    
            AND parsing_schema_name NOT IN (
                'ANONYMOUS',
                'APEX_030200',
                'APEX_040000',
                'APEX_040200',
                'APEX_SSO',
                'APPQOSSYS',
                'CTXSYS',
                'DBSNMP',
                'DIP',
                'EXFSYS',
                'FLOWS_FILES',
                'MDSYS',
                'OLAPSYS',
                'ORACLE_OCM',
                'ORDDATA',
                'ORDPLUGINS',
                'ORDSYS',
                'OUTLN',
                'OWBSYS',
                'SI_INFORMTN_SCHEMA',
                'SQLTXADMIN',
                'SQLTXPLAIN',
                'SYS',
                'SYSMAN',
                'SYSTEM',
                'TRCANLZR',
                'WMSYS',
                'XDB',
                'XS$NULL',
                'PERFSTAT',
                'STDBYPERF',
                'MGDSYS',
                'OJVMSYS'
            )
            AND plsexec_time_delta = 0
            AND executions_delta > 0
        GROUP BY
            parsing_schema_name,
            module,
            sql_id,
            plan_hash_value
    ) a
ORDER BY
    1 DESC