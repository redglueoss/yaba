﻿import traceback
import os

from django.conf import settings

import pandas as pd

class Tab:

    def __init__(self, name) :
        self.name = name

    def get_class(self) :
        return self.name.upper().replace(' ', '_')

    def get_name(self) :
        return self.name

    def get_type(self) :
        return self.__class__.__name__

    def get_data(self) :
        tab = {}
        tab['type'] = self.get_type()
        tab['class'] = self.get_class()
        tab['name'] = self.get_name()

        return tab

    def is_scored() :
        return False
    
    

class SimpleOracleTab (Tab) :
    
    def __init__(self, name, connection, sqlfile, recommendation_function=None, schema=None) :
        super().__init__(name)
        self.connection = connection
        self.sqlfile = sqlfile
        self.recommendation_function = recommendation_function
        self.score = 0
        self.recommendations = []
        self.schema = schema

    def get_data(self) :

        try:

            print('I: ++ Getting data for ' + self.get_name())

            # get query and convert to pandas dataframe
            print(os.path.join(settings.BASE_DIR, self.sqlfile))
            f = open(os.path.join(settings.BASE_DIR, self.sqlfile))
            sql_query = f.read()
            sql_query = sql_query.replace('<SCHEMA_VAL>', self.schema.upper())
            print ("Executing query: " + sql_query)
            # replace the schema here 
            df = pd.read_sql(sql_query, con=self.connection)

            # get data
            table_data = {}
            table_data['columns'] = df.columns.values.tolist()
            table_data['values'] = df.values.tolist()

            if self.recommendation_function is not None :
                self.recommendations = self.recommendation_function(df)
                recommendation_data = []
                for recommendation in self.recommendations :
                    recommendation_data.append(recommendation.get_data())
            else :
                recommendation_data = None

            tab = super().get_data()
            tab['table_data'] = table_data
            tab['recommendation_data'] = recommendation_data

            self.score = self.calculate_score()

            return tab

        except Exception as e:
            traceback.print_exc()
            raise Exception('E: Error getting data for ' + self.sqlfile)

    def calculate_score(self) :
        score = 0
        for recommendation in self.recommendations :
            score += recommendation.get_score()
            
        return score / len(self.recommendations)

    def get_score(self) :
        return self.score

    def is_scored(self) :
        return True