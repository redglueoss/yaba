﻿class Recommendation :

    def __init__(self, name, message, score) :
        self.name = name
        self.message = message
        self.score = score

    def get_class(self) :
        return 'rec-' + self.name.lower().replace(' ', '-')

    def get_name(self) :
        return self.name

    def get_message(self) :
        return self.message

    def get_score(self) :
        return self.score

    def get_data(self) :
        recommendation = {}
        recommendation['class'] = self.get_class()
        recommendation['name'] = self.get_name()
        recommendation['message'] = self.get_message()
        recommendation['score'] = self.get_score()

        return recommendation