from django.shortcuts import render
from django.template import loader
import logging

from main.yaba import *

logger = logging.getLogger(__name__)

def index(request):
    template = loader.get_template('main/results.html')

    source = request.POST.get("source", "")
    target = request.POST.get("target", "")

    if (source + target != '') :
        
        yaba_data = {}
        yaba_data['source'] = source
        yaba_data['target'] = target


        if (source == "oracle") :
            hostname = request.POST.get("hostname", "")
            port = request.POST.get("port", "")
            database = request.POST.get("database", "")
            user = request.POST.get("user", "")
            password = request.POST.get("password", "")
            schema = request.POST.get("schema", "")

            if (hostname + port + database + user + password + schema != '') :
                 yaba_data['schema'] = schema
                 yaba_data['connection_string'] = user + '/' + password + '@' + hostname + ':' + port + '/' + database
            else :
                 yaba_data['connection_string'] = None
        else :
            yaba_data['connection_string'] = None
    else :
        yaba_data = None


    report = yaba(yaba_data)

    return render(request, 'main/results.html', report)
