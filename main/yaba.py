## START YABA CODE - EXTREMELY WIP ##
from django.conf import settings

from main.models import OracleDatabase, Setting

import sys
import cx_Oracle
import traceback
from datetime import datetime
import platform
import io
from shutil import copyfile

from main.classes.Tabs import *
from main.classes.Recommendation import *


# read configuration file
def ConfigSectionMap(Config, section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint('skip: %s' % option)
        except:
            print(('exception on %s!' % option))
            dict1[option] = None
    return dict1


def ConnectOracle(conn_string):
    try:
        print('Trying ' + conn_string + ' ....')
        connection = cx_Oracle.connect (conn_string)
        print('Connected')
        return connection
    except cx_Oracle.DatabaseError:
        traceback.print_exc()
        raise Exception('Error connecting to the Database.')

def getSettingFromDB(setting) :
    return Setting.objects.filter(name=setting)[0].get_value()

### RECOMENDATIONS
# Recommendation colors on the website are based on their name,
# the class being rec-(name lowercase) with spaces replaced with '-'
# for example, "Not OK" has its class named 'rec-not-ok'
def getRecomendationSizeOracleDatabricks(data):
    db_max_size = getSettingFromDB('oracle_db_max_size')

    data_row = data.loc[data['FILE_TYPE'] == 'Data'].values.tolist()[0]

    recommendations = []
    if data_row[3] < int(db_max_size):
        name = 'Warning'
        message = 'Your total Database size (DATA) ' + str(data_row[3]) + ' Gb is small! Small amounts of data benefit less of Databricks Delta and Spark capabilities.'
    else:
        name = 'OK'
        message = 'Your total Database size (DATA) is big enough for considering an Databricks Delta offload.'

    score = round(min(data_row[3] / 1024 * 100, 100),2)

    recommendation = Recommendation(name, message, score)
    recommendations.append(recommendation)

    return recommendations


def getRecomendationOracleInfoDatabricks(data):
    #db_max_size = getSettingFromDB('oracle_db_max_size')
    
    data_row = data['INSTANCE_NUMBER'].values.tolist()[0]
    
    recommendations = []
    if data_row > 1:
        name = 'OK'
        message = 'Your Database is Oracle RAC. Oracle RAC Databases usually have more parallelism at CPU level and can benefit from Databricks/Spark design. Please consider checking the Database Parallelism section to get more information.'
        score = 100.0
    else:
        name = 'Warning'
        message = 'Your Database is not Oracle RAC. Oracle RAC Databases usually have more parallelism at CPU level and can benefit from Spark design. Please consider check Database Parallelism section to get more information.'
        score = 50.0

    recommendation = Recommendation(name, message, score)
    recommendations.append(recommendation)

    return recommendations

def getRecomendationTablespacesOracleDatabricks(data):
    tbs_max_size = getSettingFromDB('oracle_tbs_max_size')

    data_rows = data.loc[~data['TABLESPACE_NAME'].isin(['SYSTEM', 'SYSAUX', 'Total', 'UNDOTBS1'])].values.tolist()

    recommendations = []
    for row in data_rows :
        if row[1] < int(tbs_max_size):
            name = 'Warning'
            message = 'Your tablespace ' + str(row[0]) + ' is small. Small amounts of data benefit less of Databricks Delta and Spark capabilities.'
        else:
            name = 'OK'
            message = 'Your tablespace ' + str(row[0]) + ' is a good candidate for small Databricks offload. More data can benefits from Spark and Databricks architecture.'

        score = round(min(row[1] / 1024 * 100, 100),2)
        recommendation = Recommendation(name, message, score)
        recommendations.append(recommendation)

    return recommendations

def getRecomendationDMLOracleDatabricks(data):
    #tbs_max_size = getSettingFromDB('oracle_tbs_max_size')
    recommendations = []
    if (data.shape[0]) > 0:
        mean_dml = data['TOTAL_UPDATE_DELETE'].mean()
        name = 'Warning'
        message = 'Tables with lot of DML (Update and Deletes) are not the best candidates for offloading. If you consider moving any heavy DML table consider using Databricks Delta ACID features.'
    else:
        mean_dml = 0
        name = 'Info'
        message = 'Tables with less DML (Update and Deletes) are not the best candidates for offloading. If you consider moving any heavy DML table consider using Databricks Delta ACID features.'

    score = round(max((1 - mean_dml / 10000) * 100, 0),2)
    recommendation = Recommendation(name, message, score)
    recommendations.append(recommendation)

    return recommendations

def getRecomendationFTSOracleDatabricks(data):
    #tbs_max_size = getSettingFromDB('oracle_tbs_max_size')

    data_row = data['OPERATION']
    recommendations = []
    if data_row.shape[0] >= 1:
        name = 'Info'
        message = 'Tables with lot of Full Table Scans are the best candidates for Databricks/Spark Offloading, due column projection and predicate push down filters.'
        
    else:
        name = 'Warning'
        message = 'No Full Table Scans found. Tables with lot of Full Table Scans are the best candidates for Databricks/Spark Offloading, due column projection and predicate push down filters.'
        
    
    score = round(min(data_row.shape[0] / 1000 * 100, 100),2)
    recommendation = Recommendation(name, message, score)
    recommendations.append(recommendation)

    return recommendations

def getRecomendationParallelOracleDatabricks(data):
    #tbs_max_size = getSettingFromDB('oracle_tbs_max_size')

    data_row = data.loc[data['PARALLEL_OPERATION'] == 'queries parallelized'].values.tolist()[0]
    recommendations = []
    parallel_querys = int(data_row[1])
    if parallel_querys > 0:
        name = 'OK'
        message = 'Your Database doesn\'t have any parallelized querys registered. OLTP (Transactional Databases) are not recommended for Databricks and Spark workloads.'
    else:
        name = 'Warning'
        message = 'Tables with lot of Parallel DML are the best candidate for Databricks and Spark architectures.'

    score = round(min(parallel_querys / 10000 * 100, 100),2)
    
    recommendation = Recommendation(name, message, score)
    recommendations.append(recommendation)

    return recommendations

def getRecomendationTablesDopOracleDatabricks(data):
    #tbs_max_size = getSettingFromDB('oracle_tbs_max_size')
    data_row = data['3-4']
    recommendations = []
    if data_row.shape[0] >= 1:
        name = 'OK'
        message = 'Tables with lot of forced parallelism are the best candidates for Databricks/Spark offloading due is size and DML nature (FTS and Index Fast Full Scans).'
    else:
        name = 'Info'
        message = 'No tables with forced parallelism detected. Automatic DOP can still provide parallelism even if forced parallelism is not set at table level.'

    score = round(min(int(data_row.shape[0]) / 10000 * 100, 100),2)
    
    recommendation = Recommendation(name, message, score)
    recommendations.append(recommendation)

    return recommendations


def getRecomendationEncryptionOracleDatabricks(data):
    
    data_row = data['COLUMN_NAME']
    recommendations = []
    if data_row.shape[0] > 1:
        name = 'Warning'
        message = 'Some of your columns are encrypted. You have column level encryption enabled. Column level encryption is not possible in typical file formats used by Databricks as Parquet. Consider using Azure Data Lake Store encryption.'
    else:
        name = 'Info'
        message = 'No column level encryption detected. We still recomend storage encryption on Azure Data Lake Store.'
    
    score = round(max((1 - int(data_row.shape[0]) / 10000) * 100, 0),2)
    
    recommendation = Recommendation(name, message, score)
    recommendations.append(recommendation)

    return recommendations


def getRecomendationPLSQLOuterJoinsOracleDatabricks(data):
    
    data_row = data['NAME']
    recommendations = []
    if data_row.shape[0] >= 1:
        name = 'Not OK'
        message = 'You have SQL Outer Joins that are not supported in Spark SQL. When moving to Databricks and Spark use ANSI SQL.'
    else:
        name = 'OK'
        message = 'All your SQL seems standard.'

    
    score = round(max((1 - int(data_row.shape[0]) / 100) * 100, 0),2)
    
    
    recommendation = Recommendation(name, message, score)
    recommendations.append(recommendation)

    return recommendations


def getRecomendationDataTypesConvertionOracleDatabricks(data):
    
    data_row = data.loc[data['DATA_TYPE'] == 'NUMBER'].values.tolist()    
    recommendations = []
    if (len(data_row)) > 100:
        name = 'Warning'
        message = 'Your tables contains a lot of NUMBER datatypes. Please consider the suggested datatypes bellow for Databricks Delta tables.' #TODO
    else:
        name = 'OK'
        message = 'You have less than 100 NUMBER datatypes in your tables. Still consider the suggested datatypes bellow for Databricks Delta tables.' #TODO

    score = round(max((1 - len(data_row) / 100) * 100, 0),2)
    
    recommendation = Recommendation(name, message, score)
    recommendations.append(recommendation)

    return recommendations

def getRecomendationPartitionsOracleDatabricks(data):
    
    data_row = data.loc[data['PARTITIONING_TYPE'] == 'HASH'].values.tolist()    
    recommendations = []
    if (len(data_row)) >= 1:
        name = 'Warning'
        message = 'Your tables contains HASH partitioning. Databricks Delta cannot support HASH partitions.' #TODO
    else:
        name = 'OK'
        message = 'Consider partition large tables using dates or lists. It can improve Databricks Delta scans' #TODO

    score = round(min(int(len(data_row)) / 50 * 100, 100),1)
    
    recommendation = Recommendation(name, message, score)
    recommendations.append(recommendation)

    return recommendations


def get_summary_score(tabs) :
    score = 0
    tab_count = 0
    for tab in tabs :
        if tab.is_scored() :
            score += tab.get_score()
            tab_count += 1

    return round(score / tab_count)

def yaba(yaba_data=None):

    print('# YABA - Yet Another Big Data Workload Advisor #')
    print('# Redglue - This is an internal tool. Use at your own risk.')
    print('I: Please wait...')

    # report data
    report = {}
    report['report_error'] = 'none'
    report['tabs'] = []
    report['summary_score'] = 0
    report['summary_score_count'] = 0

    report['report_time'] = datetime.now().strftime("%Y-%m-%d %H:%M")
    report['report_platform'] = platform.system() + ' ' + platform.release()

    if yaba_data == None :
       return

    if yaba_data['connection_string'] == None : # TODO REMOVE THIS AND HANDLE MORE SOURCES
        report['report_error'] = 'No data provided.'
        return report

    
    # connection and cursor
    try :
        connection = ConnectOracle(yaba_data['connection_string'])
        schema = yaba_data['schema']

        tabs = []
        # database information
        database_information = SimpleOracleTab('Database Information',
                                               connection,
                                               'oracle-sql/database_info.sql',
                                               getRecomendationOracleInfoDatabricks, schema)
        tabs.append(database_information)

        # database size
        database_size = SimpleOracleTab('Database Size',
                                        connection,
                                        'oracle-sql/database_size.sql',
                                        getRecomendationSizeOracleDatabricks, schema)
        tabs.append(database_size)

        # database tablespaces
        database_tablespaces = SimpleOracleTab('Database Tablespaces',
                                               connection,
                                               'oracle-sql/database_tablespaces.sql',
                                               getRecomendationTablespacesOracleDatabricks, schema)
        tabs.append(database_tablespaces)

        # column level encryption
        database_encryption = SimpleOracleTab('Column Level Encryption',
                                              connection,
                                              'oracle-sql/database_encryption.sql',
                                              getRecomendationEncryptionOracleDatabricks, schema)
        tabs.append(database_encryption)

        # oracle dml
        database_dml = SimpleOracleTab('Oracle DML',
                                       connection,
                                       'oracle-sql/database_dml.sql',
                                       getRecomendationDMLOracleDatabricks, schema)
        tabs.append(database_dml)

        # oracle full table scans
        database_fts = SimpleOracleTab('Oracle Full Table Scans',
                                       connection,
                                       'oracle-sql/database_fts.sql',
                                       getRecomendationFTSOracleDatabricks, schema)
        tabs.append(database_fts)

        # database parallelism
        database_dop_param = SimpleOracleTab('Database Parallelism',
                                             connection,
                                             'oracle-sql/database_dop_param.sql',
                                             getRecomendationParallelOracleDatabricks, schema)
        tabs.append(database_dop_param)

        # oracle tables d.o.p
        tables_withDOP = SimpleOracleTab('Oracle Tables D.O.P',
                                         connection,
                                         'oracle-sql/tables_withDOP.sql',
                                         getRecomendationTablesDopOracleDatabricks, schema)
        tabs.append(tables_withDOP)

        # oracle non-ansi sql
        plsql_outerjoins = SimpleOracleTab('Oracle Non-ANSI SQL',
                                           connection,
                                           'oracle-sql/plsql_outerjoins.sql',
                                           getRecomendationPLSQLOuterJoinsOracleDatabricks, schema)
        tabs.append(plsql_outerjoins)

        # oracle datatypes
        tables_datatypes_conversion = SimpleOracleTab('Oracle Datatypes',
                                                      connection,
                                                      'oracle-sql/tables_datatypes_conversion.sql',
                                                      getRecomendationDataTypesConvertionOracleDatabricks, schema)
                                                    
        tabs.append(tables_datatypes_conversion)

         # oracle partitions
        tables_partitions = SimpleOracleTab('Oracle Partitions',
                                                      connection,
                                                      'oracle-sql/tables_partitions.sql',
                                                      getRecomendationPartitionsOracleDatabricks, schema)

        tabs.append(tables_partitions)

        for tab in tabs :
            report['tabs'].append(tab.get_data())

        report['score'] = get_summary_score(tabs)

    except Exception as e :
        traceback.print_exc()
        report['report_error'] = e
        return report


    return report