from django.db import models
import weakref

class OracleDatabase(models.Model) :
    name = models.CharField(max_length=200)

    user = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    hostname = models.CharField(max_length=200)
    port = models.PositiveIntegerField()
    database = models.CharField(max_length=200)

    def __str__(self):
        return self.name
    
    def get_connection_string(self) :
        return self.user + '/' + self.password + '@' + self.hostname + ':' + str(self.port) + '/' + self.database

class Setting(models.Model) :
    name = models.CharField(max_length=200, unique=True)
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.name + ': ' + self.value

    def get_value(self) :
        return self.value