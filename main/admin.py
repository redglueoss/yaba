from django.contrib import admin

from .models import OracleDatabase, Setting

admin.site.register(OracleDatabase)
admin.site.register(Setting)