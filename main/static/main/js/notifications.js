function show_notification(text) {
    var x = document.getElementById("notification");
    let lbl = document.getElementById('notification-label');
    x.style.display = "block";
    lbl.innerText = text;
}

function close_notification(text) {
    var x = document.getElementById("notification");
    x.style.display = "none";
}

function show_loading() {
    var phrases = ['Contacting the aliens...',
        'Calculating migrations...',
        'Counting database rows one by one...',
        'Making sure everything is OK...',
        'YABA YABA YABA YABA YABA...',
        'Breaking the bricks...',
        'Running away from Oracle...',
        'Praising the cloud overlords...',
        'Exploding features...'
    ];
    var x = document.getElementById("loading");
    let lbl = document.getElementById('loading-label');
    x.style.display = "flex";
    lbl.innerHTML = phrases[Math.floor(Math.random() * phrases.length)];
    setInterval(function () {
        lbl.classList.add('hide');
        setTimeout(function () {
            new_phrase = phrases[Math.floor(Math.random() * phrases.length)];
            while (new_phrase == lbl.innerHTML) {
                new_phrase = phrases[Math.floor(Math.random() * phrases.length)];
            }
            lbl.innerHTML = new_phrase;
            lbl.classList.remove('hide');
        }, 500);
        
    }, 3000);
    
}