function show_percentage(p) {
    var label = document.getElementById("summary-percentage");
    var width = 0;
    var color = width / 100 * 120
    var speed = 100 / p * 20
    var id = setInterval(frame, speed);
    label.innerText = width + "%";
    label.style.color = "hsl(" + color + ", 100%, 40%)"
    function frame() {
        if (width >= p) {
            if (p >= 50) {
                label.innerText = "Workload Analysis Score:" + width + "%";
            } else {
                label.innerText = "Workload Analysis Score:" + width + "%";
            }
            clearInterval(id);
        } else {
            width++;
            color = width / 100 * 120
            label.innerText = width + "%";
            label.style.color = "hsl(" + color + ", 100%, 40%)"
        }
    }
}