var current_form = "st-form"
show_form("st-form");

function show_form(form) {
    document.getElementById(current_form).style.display = "none";
    document.getElementById(form).style.display = "block";
    if (form == "st-form") {
        document.getElementById("form-previous").style.display = "none";
    } else {
        document.getElementById("form-previous").style.display = "inline";
    }
    if (form == "st-form") {
        document.getElementById("form-next").innerHTML = "Next";
    } else {
        document.getElementById("form-next").innerHTML = "Submit";
    }

    current_form = form;
}

function next_form() {
    if (current_form == "st-form") {
        var source_input = document.getElementById("setup-source");
        var source = source_input.options[source_input.selectedIndex].value;

        document.getElementById(current_form).style.display = "none";

        current_form = source + "-form"
        show_form(current_form);

    } else {
        if (validate_form()) {
            show_loading();
            document.getElementById("setup-form").submit();
            return false;
        }
    }
    
}

function previous_form() {
    document.getElementById(current_form).style.display = "none";
    current_form = "st-form";

    show_form(current_form);

}

function validate_form() {
    var valid = true;
    var x = document.getElementById(current_form).getElementsByTagName("input");
    for (var i = 0; i < x.length; i++) {
        if (x[i].value == "") {
            x[i].className += " invalid";
            valid = false;
        }
    }

    return valid;
}