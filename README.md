YABA

**What is?**

Yaba is a datawarehouse workload analyser for RDBMS, for those who want to move the workloads for Databricks. Currently only Oracle to Databricks is supported.

**Features**
1. It supports connection to Oracle Databases (11.x to 18.3)
2. It analyzes Oracle internal metrics and do recomendations to migrations to Databricks Deltas or Spark
3. Calculate a "difficulty" score for the migration

**Current Version**

Version 1.0 (Beta)

**How to Install**

**1. Install (Django)**

1. Check your Python version. Python 3 is recommended (3.6.x or 3.7.x)
2. Make sure that a network rule (inbound/outbound) allows port 8000
2. sudo apt install python3-pip
3. pip3 install django
4. pip3 install pandas

**2. Install (cxOracle - Oracle driver)**
1. pip3 install cx_oracle
2. sudo apt-get install alien
3. sudo apt-get install libaio1
4. (Download Oracle Instant Client for Linux x86-64 RPM)
5. sudo alien -i oracle-instantclient18.5-basic-18.5.0.0.0-3.x86_64.rpm
6. Edit .bash_profile to user (redglue) and add the following:

> export LD_LIBRARY_PATH=/usr/lib/oracle/18.5/client64/lib/${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}

> export ORACLE_HOME=/usr/lib/oracle/18.5/client64

> export PATH=$PATH:$ORACLE_HOME/bin


7. If needed create a symlink:

> redglue@yabavm:/usr/lib/oracle/18.5/client64/lib$ sudo ln -s libclntsh.so.18.1 libclntsh.so 



**How to Run**
1. redglue@yabavm:~/yaba$ python3 manage.py runserver 0.0.0.0:8000
2. Update yaba/settings.py and add 
> ALLOWED_HOSTS = ['*']  
to allow any host to connect


**NOTE**
This is beta software. Use at your own risk!

